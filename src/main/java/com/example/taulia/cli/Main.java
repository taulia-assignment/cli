package com.example.taulia.cli;

import com.beust.jcommander.JCommander;
import com.example.taulia.cli.command.Command;
import com.example.taulia.cli.command.Commands;
import com.example.taulia.cli.command.DataGeneratorCommand;
import com.example.taulia.cli.command.FileProcessorCommand;

public class Main {
    public static void main(String[] args) {
        Command fileProcessor = new FileProcessorCommand();
        Command dataGenerator = new DataGeneratorCommand();

        JCommander jCommander = JCommander.newBuilder()
                .addCommand(fileProcessor)
                .addCommand(dataGenerator)
                .build();

        try {
            jCommander.parse(args);
            String command = jCommander.getParsedCommand();

            if (Commands.PROCESS.getCommand().equals(command))
                fileProcessor.execute();
            else if (Commands.GENERATE.getCommand().equals(command))
                dataGenerator.execute();

        } catch (Exception e) {
            System.out.println("Problem occurred while executing action: " + e.getMessage());
        }
    }
}
