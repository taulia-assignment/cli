package com.example.taulia.cli.command;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.example.taulia.data.processor.mapper.ModelMapperConfiguration;
import com.example.taulia.data.processor.processor.CsvFileProcessor;
import com.example.taulia.data.processor.processor.FileProcessor;
import com.example.taulia.data.processor.processor.XmlFileProcessor;

@Parameters(
        commandNames = {"process"},
        commandDescription = "Parse a CSV file, process its data, and outputs it into a specified format")
public class FileProcessorCommand implements Command {

    @Parameter(
            names = {"-p", "--path"},
            required = true)
    private String path;

    @Parameter(
            names = {"-f", "--format"},
            required = true)
    private String format;

    public void execute() throws Exception {
        ModelMapperConfiguration modelMapperConfiguration = new ModelMapperConfiguration();

        FileProcessor fileProcessor;
        if ("csv".equalsIgnoreCase(format))
            fileProcessor = new CsvFileProcessor(modelMapperConfiguration);
        else if ("xml".equalsIgnoreCase(format))
            fileProcessor = new XmlFileProcessor(modelMapperConfiguration);
        else
            throw new IllegalArgumentException("Incorrect format. Possible formats - csv/xml");

        fileProcessor.process(path);
    }
}
