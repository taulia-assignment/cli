package com.example.taulia.cli.command;

public enum Commands {
    PROCESS("process"),
    GENERATE("generate");

    private String command;

    Commands(String command) {
        this.command = command;
    }

    public String getCommand() {
        return command;
    }
}
