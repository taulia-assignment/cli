package com.example.taulia.cli.command;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.example.taulia.data.generator.DataGenerator;
import com.example.taulia.data.generator.DataGeneratorProperties;

@Parameters(
        commandNames = {"generate"},
        commandDescription = "Generate sample data")
public class DataGeneratorCommand implements Command {

    @Parameter(
            names = {"-p", "--path"},
            required = true)
    private String path;

    @Parameter(
            names = {"-n"},
            required = true)
    private int numberOfRecords;

    @Override
    public void execute() throws Exception {
        DataGenerator dataGenerator = new DataGenerator();

        dataGenerator.generate(DataGeneratorProperties.builder()
                .fileLocation(path)
                .numberOfRecords(numberOfRecords)
                .build());
    }
}
