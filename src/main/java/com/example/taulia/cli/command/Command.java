package com.example.taulia.cli.command;

public interface Command {
    void execute() throws Exception;
}
