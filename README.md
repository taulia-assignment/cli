Taulia Assignment
-
This is a CLI program that allows users to:
 * generate sample data into *csv* files (with specific structure)
 * parse *csv* (with specific structure), process its data, and outputs in *csv* or *xml*

How to run the program
- 
You will need:
* Java 8 installed and set to system path
* Maven installed and set to system path
* All three projects cloned on your computer

The first step is to install data-generator and data-processor projects into Maven local repository. This can be achieved 
by navigating to data-generator project directory, opening your terminal, and typing  ``mvn install``. The same goes for 
data-processor project, too.

The second step is to create a fat JAR from the cli project. You can do this by navigating to the cli project directory, 
opening your terminal, and typing ``mvn package``. This will create the necessary fat JAR file under /target directory.

Now you can run the application by navigating to /target, and typing:

``java -jar <jar-with-dependencies-name> [parameters]``

in your terminal.

Program arguments
-
generate -p *path-to-csv-file* -n *desired-number-of-records* --- this will create *csv* file with sample data

process -p *path-to-csv-file* -f *format (either csv or xml)* --- this will parse, process, and output the data from the
specified *csv* file into files under your users's home directory

Example usages
-
The following example will create (if not exists) sample-data.csv file and will populate it with random generated
sample data with a limit of 300 records.
 
``java -jar taulia-cli-1.0-jar-with-dependencies.jar generate -p D:\sample-data.csv -n 300``

The following example will parse and process the data from the specified file. The produced output files can be found under
your user's home directory. The -f flag can accept either csv or xml.

``java -jar taulia-cli-1.0-jar-with-dependencies.jar process -p D:\sample-data.csv -f csv``